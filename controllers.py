"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n & pluralization
@action.uses(auth.user)       indicates that the action requires a logged in user
@action.uses(auth)            indicates that the action requires the auth object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your app will result in undefined behavior
"""

import datetime
import uuid

from py4web import action, request, abort, redirect, URL, Field
from py4web.utils.form import Form, FormStyleBulma
from pydal.validators import *


from yatl.helpers import A
from . common import db, session, T, cache, auth, signed_url
from . ui import FormStyleSherit, IS_LIST_OF_EMAILS_AND_SPLIT
from . grid import Grid
from . vueform import VueForm, InsertForm, TableForm
from . fileupload import FileUpload
from . starrater import StarRater

# -----------------------------

@action('index', method='GET')
@action.uses('index.html')
def index():
    return dict()

# -----------------------------
# Test form.
def get_time():
    return datetime.datetime.utcnow()

test_form = VueForm('test_form', session,
                       [Field('name', default="Luca"),
                        Field('read', 'boolean', default=True),
                        Field('animal', requires=IS_IN_SET(['cat', 'dog', 'bird']),
                              default='dog'),
                        Field('choice', requires=IS_IN_SET({'c': 'cat', 'd': 'dog', 'b': 'bird'}),
                              default='d'),
                        Field('arrival_time', 'datetime', default=get_time),
                        ], readonly=False)

@action('test_form', method=['GET'])
@action.uses(test_form, "form.html")
def test_form_page():
    return dict(form=test_form(), redirect_url=URL('test_form'))

# -----------------------------

@action('add_form', method=['GET', 'POST'])
@action.uses('add_form.html', session, db, auth.user)
def add_form():
    """Adds a product using an entirely custom form"""
    form = Form(db.product, formstyle=FormStyleBulma)
    if form.accepted:
        # We always want POST requests to be redirected as GETs.
        redirect(URL('add_form'))
    return dict(form=form)

# -----------------------------
# GOOD

# Sample grid.
samplegrid = Grid('grid_api', session)

@action('samplegrid', method=['GET'])
@action.uses(samplegrid, 'grid.html')
def sample_grid():
    """This page generates a sample grid."""
    # We need to instantiate our grid component.
    return dict(grid=samplegrid())

# -----------------------------
# File uploader.

file_uploader = FileUpload('upload_api', session)

@action('file_uploader', method=['GET'])
@action.uses(file_uploader, 'uploader.html')
def sample_uploader():
    """Sample upload API."""
    return dict(uploader=file_uploader())

# -----------------------------
# Insertion form.
insert_form = InsertForm('insert_product', session, db.product, redirect_url='index')

@action('insert_form', method=['GET'])
@action.uses(insert_form, 'form.html')
def add_product():
    return dict(form=insert_form())

# -----------------------------
# Update form.
update_form = TableForm('update_product', session, db.product, redirect_url='index')

@action('update_form', method=['GET'])
@action.uses(update_form, 'form.html')
def update_product():
    # For simplicity, we update the record 1.
    return dict(form=update_form(id=1))

# -----------------------------
# Star rater.

star_rater = StarRater('star_rater', session)

@action('starrating_example', method=['GET'])
@action.uses(star_rater, 'starrating_example.html')
def starrating_example():
    return dict(stars=star_rater())

# -----------------------------
# Boolean form.
boolean_form = VueForm('boolean_form', session,
                       [Field('name'), Field('read', 'boolean')])

@action('boolean_page', method=['GET'])
@action.uses(boolean_form, "form.html")
def boolean_page():
    return dict(form=boolean_form())

# -----------------------------

# -----------------------------
# Insert via update form.

@action('insert_via_update_form', method=['GET'])
@action.uses(update_form, 'form.html')
def insert_via_update():
    return dict(form=update_form())

# -----------------------------

@action('testsig', method=['GET'])
@action.uses('testsig.html', session)
def testsig():
    """Test for URL signer"""
    return dict(myurl=URL('testurl', signer=signed_url))

@action('testurl', method=['GET'])
@action.uses('testurl.html', signed_url.verify())
def testurl():
    return dict()

@action('create', method=['GET', 'POST'])
@action.uses('create.html', session, db, auth.user)
def create_class():
    """Creates a class."""
    form = Form([Field('class_name')], formstyle=FormStyleSherit)
    if form.accepted:
        # Creates the class.
        class_id = db.class_table.insert(
            name=form.vars['class_name'],
            api_key=str(uuid.uuid4())
        )
        # Writes that the creator is the manager of the class.
        db.user_classes.insert(
            class_id=class_id,
            user_email=auth.current_user['email'],
            is_owner=True,
            is_manager=True,
        )
        redirect(URL('manage', class_id))
    return dict(form=form)


@action('manage/<class_id>', method=['GET','POST'])
@action.uses('manage.html', session, db, auth.user)
def manage_get(class_id=None):
    """Allows users to manage a class they have created."""
    # Reads the class.
    c = db.class_table[class_id]
    if c is None:
        redirect(URL('index'))
    # Checks that the class belongs to the user.
    m = db((db.user_classes.class_id == c.id) &
           (db.user_classes.user_email == auth.current_user['email']) &
           (db.user_classes.is_owner == True)).select().first()
    if m is None:
        redirect(URL('index'))
    # The user can edit the class.  The user can change the list of other managers.
    # Let us read the list of other managers.
    old_managers = {r.user_email for r in db((db.user_classes.class_id == c.id) &
                                             (db.user_classes.is_manager == True)).select()}
    old_managers -= {auth.current_user['email']}
    # Creates a form for editing this information.
    form = Form([
        Field('class_name'),
        Field('class_managers', 'text', requires=IS_LIST_OF_EMAILS_AND_SPLIT(),
              placeholder="Enter the list of class manager emails (in addition to yourself), separated by spaces or commas."
              ),
    ],
        record={'class_name': c.name, 'class_managers': ', '.join(old_managers)},
        deletable=False,
        formstyle=FormStyleSherit)
    if form.accepted:
        # Updates the class name.
        if form.vars['class_name'] != c.name:
            c.update_record(name=form.vars['class_name'])
        # Updates the managers.
        new_managers = set(form.vars['class_managers'])
        for m in new_managers - old_managers:
            db.user_classes.insert(user_email=m, class_id=c.id, is_manager=True)
        for m in old_managers - new_managers:
            db((db.managers_table.class_id == c.id) &
               (db.managers_table.user_email == m) &
               (db.managers_table.is_manager == True)).delete()
        redirect(URL('index'))
    return dict(c=c, form=form)
