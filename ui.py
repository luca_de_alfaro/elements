"""
UI functions: forms definitions, etc.
"""

import re
from py4web.utils.form import Form, FormStyleDefault, FormStyleBulma

from py4web import request
from yatl.helpers import A, TEXTAREA, INPUT, TR, TD, TABLE, DIV, LABEL, FORM, SELECT, OPTION, P

from py4web import Field
from pydal.validators import Validator, ValidationError, IS_EMAIL

def FormStyleDefaultWithPlaceholder(table, vars, errors, readonly, deletable, classes=None):
    form = FORM(_method='POST', _action=request.path, _enctype='multipart/form-data')

    classes = classes or {}
    class_label = classes.get('label', '')
    class_outer = classes.get('outer', '')
    class_inner = classes.get('inner', '')
    class_error = classes.get('error', '')
    class_info = classes.get('info', '')

    for field in table:

        input_id = '%s_%s' % (field.tablename, field.name)
        value = field.formatter(vars.get(field.name))
        error = errors.get(field.name)
        field_class = field.type.split()[0].replace(':','-')

        if field.type == 'blob': # never display blobs (mistake?)
            continue
        elif readonly or field.type=='id':
            if not field.readable:
                continue
            else:
                control = DIV(field.represent and field.represent(value) or value or '')
        elif not field.writable:
            continue
        elif field.widget:
            control = field.widget(table, value)
        elif field.type == 'text':
            control = TEXTAREA(value or '', _id=input_id,_name=field.name)
        elif field.type == 'boolean':
            control = INPUT(_type='checkbox', _id=input_id, _name=field.name,
                            _value='ON', _checked = value)
        elif field.type == 'upload':
            control = DIV(INPUT(_type='file', _id=input_id, _name=field.name))
            if value:
                control.append(A('download', _href=field.download_url(value)))
                control.append(INPUT(_type='checkbox',_value='ON',
                                     _name='_delete_'+field.name))
                control.append('(check to remove)')
        elif hasattr(field.requires, 'options'):
            multiple = field.type.startswith('list:')
            value = list(map(str, value if isinstance(value, list) else [value]))
            options = [OPTION(v,_value=k,_selected=(not k is None and k in value))
                       for k, v in field.requires.options()]
            control = SELECT(*options, _id=input_id, _name=field.name,
                              _multiple=multiple)
        else:
            field_type = 'password' if field.type == 'password' else 'text'
            control = INPUT(_type=field_type, _id=input_id, _name=field.name,
                            _value=value, _class=field_class)

        key = control.name.rstrip('/')
        if key == 'input':
            key += '[type=%s]' % (control['_type'] or 'text')
        control['_class'] = classes.get(key, '')
        if hasattr(field, 'placeholder'):
            control['_placeholder'] = field.placeholder

        form.append(DIV(
                LABEL(field.label, _for=input_id, _class=class_label),
                DIV(control, _class=class_inner),
                P(error, _class=class_error) if error else '',
                P(field.comment or '', _class=class_info),
                _class=class_outer))

    if deletable:
        form.append(DIV(DIV(INPUT(_type='checkbox',_value='ON',_name='_delete',
                                  _class=classes.get('input[type=checkbox]')),
                            _class=class_inner),
                        P('check to delete',_class="help"),
                        _class=class_outer))
    submit = DIV(DIV(INPUT(_type='submit',_value='Submit',
                           _class=classes.get('input[type=submit]')),
                     _class=class_inner),
                 _class=class_outer)
    form.append(submit)
    return form


def FormStyleSherit(table, vars, errors, readonly, deletable):
    classes = {
        'outer': 'field',
        'inner': 'control',
        'label': 'label',
        'info': 'help',
        'error': 'help is-danger',
        'submit': 'button',
        'input': 'input',
        'input[type=text]': 'input',
        'input[type=radio]': 'radio',
        'input[type=checkbox]': 'checkbox',
        'input[type=submit]': 'button is-primary',
        'select': 'select',
        'textarea': 'textarea',
        }
    return FormStyleDefaultWithPlaceholder(table, vars, errors, readonly, deletable, classes)


class IS_LIST_OF_EMAILS_AND_SPLIT(Validator):
    """
    Similar to IS_LIST_OF_EMAILS, but also splits the emails.
    """
    REGEX_NOT_EMAIL_SPLITTER = r'[^,;\s]+'

    def __init__(self, error_message='Invalid emails: %s'):
        self.error_message = error_message

    def validate(self, value, record_id=None):
        bad_emails = []
        f = IS_EMAIL()
        result = []
        for email in re.findall(self.REGEX_NOT_EMAIL_SPLITTER, value):
            error = f(email)[1]
            if error and email not in bad_emails:
                bad_emails.append(email)
            else:
                result.append(email)
        if bad_emails:
            raise ValidationError(self.translator(self.error_message) % ', '.join(bad_emails))
        return result

    def formatter(self, value, row=None):
        return ', '.join(value or [])
